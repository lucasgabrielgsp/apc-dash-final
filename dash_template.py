import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from funcoes import * #importando as funções do arquivo funcoes.py
import plotly.express as px

import pandas as pd
from unidecode import unidecode



############ dados
data = pd.read_csv('/home/lucas/apc-redes-sociais/TheSocialDilemma.csv')
df_contagem = pd.read_csv('/home/lucas/apc-redes-sociais/df_contagem.csv')
new_data = filtragem(data)

########## mapa
fig = mapa(df_contagem)

# Os parametros da barra lateral (CSS)
SIDEBAR_STYLE = {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'bottom': 0,
    'width': '20%',
    'padding': '20px 10px',
    'background-color': '#f8f9fa'
}

# Parametros da página principal (css)
CONTENT_STYLE = {
    'margin-left': '25%',
    'margin-right': '5%',
    'padding': '20px 10px'
}

TEXT_STYLE = {
    'textAlign': 'center',
    'color': '#191970'
}



controls = dbc.FormGroup(
    [

        html.P('Países', style={
            'textAlign': 'center'
        }),
        dbc.Card([dbc.RadioItems(
            id='paises',
            options=[{
                'label': 'Estados Unidos',
                'value': 'USA'
            },
                {
                    'label': 'India',
                    'value': 'IND'
                },
                {
                    'label': 'Canadá',
                    'value': 'CAN'
                },
                {
                    'label': 'Inglaterra',
                    'value': 'GBR'
                },
                {
                    'label': 'Quênia',
                    'value': 'KEN'
                },
                {
                    'label': 'Espanha',
                    'value': 'ESP'
                },

            ],
            value='USA', #valor inicial na dashboard
            style={
                'margin': 'auto'
            }
        )]),
 
        html.Br(), #quebra de linha
        dbc.Button(
            id='submit_button',
            n_clicks=0,
            children='Filtrar',
            color='primary',
            block=True
        ),
    ]
)

sidebar = html.Div(
    [
        html.H2('Filtros', style=TEXT_STYLE),
        html.Hr(),
        controls
    ],
    style=SIDEBAR_STYLE,
)



content_first_row = dbc.Row(
    [
        dbc.Col(
            dcc.Graph(figure = fig), md=12,
        )
        
        
    ]
)


content_second_row = dbc.Row(
    [
        dbc.Col(
            dcc.Graph(id='graph_1'), md=4
        ),
        dbc.Col(
            dcc.Graph(id='graph_2'), md=4
        ),
        dbc.Col(
            dcc.Graph(id='graph_3'), md=4
        )
    ]
)


content = html.Div(
    [
        html.H2('DashBoard The Social Dilemma', style=TEXT_STYLE),
        html.Hr(),
        content_first_row,
        content_second_row,
       
    ],
    style=CONTENT_STYLE
)

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
app.layout = html.Div([sidebar, content])

    

@app.callback(
    Output('graph_1', 'figure'),
    [Input('submit_button', 'n_clicks')],
    [
     State('paises', 'value')
     ])
def update_graph_1(n_clicks,  paises_value):
    return graphic_sentiment(paises_value,new_data)


@app.callback(
    Output('graph_2', 'figure'),
    [Input('submit_button', 'n_clicks')],
    [
     State('paises', 'value')
     ])
def update_graph_2(n_clicks,  paises_value):
    
    return graphic_disp(paises_value,new_data)


@app.callback(
    Output('graph_3', 'figure'),
    [Input('submit_button', 'n_clicks')],
    [
     State('paises', 'value')
     ])
def update_graph_3(n_clicks,paises_value):
    print(n_clicks)
    print(paises_value)
    
    return graphic_veri(paises_value,new_data)


if __name__ == '__main__':
    app.run_server(port='8085')
