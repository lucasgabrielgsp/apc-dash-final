import plotly.express as px
from unidecode import unidecode
import pandas as pd

def filtragem(data):
  locations = [] 
  iso = px.data.gapminder()
  unidecode_country = []
  paises = []
  user_location_clean = []
  idx =  [] 
  i = 0
  iso_alfa = []
  

  contagem_pais = dict() 
  contagem_iso = dict()

  for item in data.user_location: 
    locations.append(unidecode(str(item)).upper())
  for item in iso.country:
    unidecode_country.append(unidecode(item).upper())

  iso['country'] = unidecode_country

  for pais in iso.country:
    if pais not in paises:
        paises.append(pais)

  for location in locations: 
    for pais in paises: 
        if pais in location: 
            user_location_clean.append(pais) 
            idx.append(i) 
    i=i+1    

  for item in user_location_clean:
    iso_alfa.append(iso[iso.country==item].iloc[0].iso_alpha)

  new_data = data.iloc[idx,:].reset_index(drop=True)
  new_data['country'] = user_location_clean
  new_data['iso_alpha'] = iso_alfa

  #alterando os dispositivos
  dispositivo = new_data['source']
  for i in range(0,len(dispositivo)):
      if 'Twitter for iPhone' not in dispositivo[i] and 'Twitter for Android' not in dispositivo[i] and 'Twitter Web App' not in dispositivo[i]:
        dispositivo[i] = 'Outros'
  new_data['source'] = dispositivo
  new_data = new_data[['user_name', 'user_location', 'country', 'iso_alpha', 'user_description',
                     'user_created', 'user_followers', 'user_friends', 'user_favourites', 'user_verified',
                     'date', 'text', 'hashtags', 'source', 'is_retweet', 'Sentiment']]
  
  return new_data


def mapa(df_contagem):
  fig = px.scatter_geo(df_contagem, locations='iso_alpha',
                     hover_name='country', size='count',
                     projection='natural earth')

  return fig
  
def graphic_sentiment(country,new_data):
  selected_country = new_data[new_data['iso_alpha'] == country] # filtrando o pais

  #Sentimento
  fig_s = px.pie(selected_country, names = 'Sentiment', color_discrete_sequence= ['#66b366',' #b9b9b9','#ff4d4d'])
  fig_s.update_traces(hoverinfo='label+percent', textfont_size=15,marker=dict(line=dict(color='black', width=0.5)))
  fig_s.update_layout(margin=dict(t=34, b=34, l=34, r=34),height=500, showlegend = True,legend=dict(yanchor="top",y=0.99,xanchor="left",x=0.01),title={'text': "Sentimento",'y':0.99,'x':0.5,'xanchor': 'right','yanchor':'top'})
  return fig_s

def graphic_veri(country,new_data):
  selected_country = new_data[new_data['iso_alpha'] == country] # filtrando o pais

  #Verificado/não verificado
  fig_v = px.pie(selected_country, names='user_verified', color='user_verified',color_discrete_sequence= ['#ff4d4d','#66b366'])
  fig_v.update_traces(hoverinfo='label+percent', textfont_size=15, marker=dict(line=dict(color='black',width=0.5)))
  fig_v.update_layout(margin=dict(t=34, b=34, l=34, r=34),height=500, showlegend =True, legend=dict(yanchor="top",y=1.1,xanchor="left",x=0.01),title={'text': "Tipos de usuário",'y':0.99,'x':0.5,'xanchor': 'right','yanchor':'top'})

  return fig_v

def graphic_disp(country,new_data):
  selected_country = new_data[new_data['iso_alpha'] == country] # filtrando o pais

  #Dispositivos
  fig_so = px.pie(selected_country, names = 'source', color_discrete_sequence = px.colors.sequential.Inferno_r)
  fig_so.update_traces(hoverinfo='label+percent', textfont_size=15, marker=dict(line=dict(color='black', width=0.5)))
  fig_so.update_layout(margin=dict(t=34, b=34, l=34, r=34),height=500, showlegend = True,legend=dict(yanchor="top",y=1.1,xanchor="left",x=0.01),title={'text': "Dispositivos",'y':0.99,'x':0.5,'xanchor': 'right','yanchor':'top'})
  return fig_so
